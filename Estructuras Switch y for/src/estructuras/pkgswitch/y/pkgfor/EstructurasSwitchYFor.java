
package estructuras.pkgswitch.y.pkgfor;

import java.util.Scanner;


public class EstructurasSwitchYFor {
// Imprime el mensaje 
    static void imprimirMensaje(String sMensaje){
        System.out.println(sMensaje);
    }
    static void separador(){
        imprimirMensaje("----------------------------------------------------");
    }
    //Información
    static void encabezado(){
        imprimirMensaje("Universidad Autónoma de Campeche");
        imprimirMensaje("Facultad de Ingeniería");
        imprimirMensaje("Ingeniería en Sistemas Computacionales");
        imprimirMensaje("ID 57039 Jessica Johana Estefanía. 2A");
        separador();
    }
    //Menú
    static void menu(){
        Scanner lector = new Scanner (System.in);
        imprimirMensaje("Ingrese una opción:\n");
        imprimirMensaje("1.Imprimir la suma consecutiva del 0 al 10 (FOR 1)");
        imprimirMensaje("2.Crear un metodo que lea un valor entero del 1 al 12...(SWITCH)");
        imprimirMensaje("3.Código Morse");
        imprimirMensaje("\nTecleé el número que eligió");
        int iOpcion = lector.nextInt();
        submenu(iOpcion);
    }
   //Submenú
    static void submenu(int iOpcion){
        separador();
        if (iOpcion == 1){
            imprimirMensaje("Imprimir la suma consecutiva del 0 al 10 (FOR 1) Ejercicio 1");            
            eNum1();
            separador();
        } if (iOpcion == 2){
            imprimirMensaje("Crear un metodo que lea un valor entero del 1 al 12...(SWITCH) Ejercicio 5");
            eNum5();
            separador();
        } if (iOpcion == 3){
            imprimirMensaje("Código Morse(FOR 2) Ejercicio 9");
            eNum9();
            separador();
        } 
    } 
    
    //Imprimir la suma consecutiva del 0 al 10 (FOR 1) EJERCICIO 1
    static void eNum1(){
   
        int suma=0;
        for (int i=0; i<=10; i++){
            suma=suma+i;
        }
        System.out.println("La suma es: " + suma);
    }  
    
    //Crear un metodo que lea un valor entero del 1 al 12... (SWITCH) EJERCICIO 5
    static void eNum5(){
        Scanner lector = new Scanner (System.in);
        System.out.println("Ingrese un número del 1 al 12");
        int aNum= lector.nextInt();
        
        switch(aNum){
        case 1:
        case 2:
        case 3:
            System.out.println("Primer trimestre");break;
        case 4:
        case 5:
        case 6:
            System.out.println("Segundo trimestre");break;
        case 7:
        case 8:
        case 9:
            System.out.println("Tercer trimestre");break;
        case 10:
        case 11:
        case 12:
            System.out.println("Cuarto trimestre");break;
        default: 
            System.out.println("El número ingresado no es válido");break;
        }   
    }
    
    //Imprimir letra y su código Morse asignado (FOR 2) EJERCICIO 9
    static void eNum9(){
        String[] aLetraym= {"A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z",".-","-...","-.-.","-..",".","..-.","--.","...","..",".---","-.-",".-..","--","-.","---",".--.","--.-",".-.","...","-","..-","...-",".--","-..-","-.--","--.."};
        String [][] adeAmbos= {{aLetraym[0],aLetraym[26]},{aLetraym[1],aLetraym[27]},{aLetraym[2],aLetraym[28]},{aLetraym[3],aLetraym[29]},{aLetraym[4],aLetraym[30]},{aLetraym[5],aLetraym[31]},{aLetraym[6],aLetraym[32]},{aLetraym[7],aLetraym[33]},{aLetraym[8],aLetraym[34]},{aLetraym[9],aLetraym[35]},
        {aLetraym[10],aLetraym[36]},{aLetraym[11],aLetraym[37]},{aLetraym[12],aLetraym[38]},{aLetraym[13],aLetraym[39]},{aLetraym[14],aLetraym[40]},{aLetraym[15],aLetraym[41]},{aLetraym[16],aLetraym[42]},{aLetraym[17],aLetraym[43]},{aLetraym[18],aLetraym[44]},{aLetraym[19],aLetraym[45]},
        {aLetraym[20],aLetraym[46]},{aLetraym[21],aLetraym[47]},{aLetraym[22],aLetraym[48]},{aLetraym[23],aLetraym[49]},{aLetraym[24],aLetraym[50]},{aLetraym[25],aLetraym[51]}};
 
        for (int i=0; i<26; i++){ //núm de filas
           for (int j=0; j<2; j++){ //núm de columnas
            System.out.print(adeAmbos[i][j]);
            if (j==0) System.out.print(" = ");
        }
            System.out.println("");
          }
    }
    public static void main(String[] args) {
       //Funciones
       encabezado();  
       menu();
    }
    
}
